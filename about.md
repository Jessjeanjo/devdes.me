---
layout: page
title: About
permalink: /about/
---

Bonjour! 

I'm Jessica Joseph, a Computer Engineering student at New York University. I have a passion for developing and designing!

### More Information

In my spare time, I am a total Lynda junkie. I loved enhancing my skills and exploring new areas. I can be found tinkering with logo designs on Illustrator, trying to charm the snake(Python Programming joke), at art galleries, tech meetups around the city, or at the gym. 
### Contact me

[Jessjeanjo@gmail.com](mailto:Jessjeanjo@gmail.com)
