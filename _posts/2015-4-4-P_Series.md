---
layout: post
title: P- SERIES "THE END IS NEAR" REFLECTIONS
---

With my First Year at NYU coming to an end, I have been reflecting <span class="color">tremendously</span>. 
Within this relatively short period of time, I have experienced so much growth spiritually, mentally, emotionally. So much change has taken place. 

As a way to kick off <span class="color">Dev | Des</span>,  my developer, designer,and entrepreneur blog ,
I am going to be going through my reflections and what is to come!<br><br>Until next time,<br>~Au Revoir, mes amis~
