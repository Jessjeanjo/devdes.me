---
layout: post
title: Intel Hackathon Meet Up
---

The Intel Hackathon was held at Impact Hub on Broadway. The was almost exactly as I thought it would be-- the 21st century stereotypical tech innovation lab, minus the super tiny elevators, I wasn't expecting that. So small.

The meet up started with check in and swag! Swag is always nice. Afterwards, I began speaking with a professor from Columbia and  
@NikkiFinnemore, the 3D Hubs representative of the evening.

It was interesting seeing the variety in age that a Hackathon could have. I initially thought Hackathons were just full of teenage boys, but there was actually a nice mix age wise. There were even quite a bit of females there as well.

Best part of the night would have to be actually holging/playing with an Intel Edison. One of the guys I was talking to actually had an Intel Edison with him, like whattt. I felt like a kid in a toy store. It's moments like that, that almost confirm I'm in the right area.

After an Intel Rep spoke about the Intel Edison and what would happen at the hackathon, ideas were pitched, and attendees were given a chance to mingle, exchange info, start to team up.

During the mingling segment while I was engaging with the people who pitched, I started realizing that I'm going to have to pretty much define what type of environment I want for the Hackathon. Sounds weird, I know, but after the Intersections Retreat I feel more aware about what happens in my life. Like, for the hackathon do I want to really step back a little and mainly learn surrounding myself with much more experienced people or do I want to just dive right in with other half and half experienced people and struggle as we go along. Each of which has its pros and cons, of course, but I'm not sure. I want to just dive in, but learning could also be fantastica as well especially since this would be my first hardware/software hackathon.

All in all, I'm looking forward to working with the Intel Edison. This should be fun! 

Until next time,
Au Revoir, mes amis~~
