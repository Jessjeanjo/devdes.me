---
layout : post
title : THE END IS NEAR_Pathways
---

My largest educational change was changing my major. But emotionally and socially, no change is greater than the change 
I had with my Pathways Cohort.<br><br><i><span class="color">So, what's pathways?</span></i>

The Pathways program is an opportunity for first-year commuter students to engage in experiential learning outside 
the classroom with a faculty member. There were two themes to choose from Internationally Known: NYC in the Global Context 
and Street SmARTS: From Pencils to Portraits. Based on the theme I chose and got accepted to, I was assigned both a Commuter 
Assistant and Faculty Affiliate.

Typically, the commuters cohorts are school based, i.e. students who go to Tisch would have a commuters assistant that 
also goes to Tisch. But, Pathways are themed commuters cohorts so first year students from Poly can interact with first year
students from Tisch, Liberal Studies, CAS, Stern, etc. It's mixed. <strong><span class="color">I was part of Street SmARTS and I 
wouldn't change a thing!!</span></strong>

<img src="https://scontent-1.2914.fna.fbcdn.net/hphotos-xaf1/v/t1.0-9/10406397_696044417142675_6145597613626164863_n.jpg?oh=5fc03839e7b09081aed4b8f450a1b7e8&oe=55B24D12" 
alt="Pathways">

My pathways cohort is comprised of Poly students, Liberal Studies students, and CAS students. And it is just amazing how 
in the beginning we were just a group of confused, scared little freshman with an amazingly welcoming and outgoing Commuter's
Assistant, Diler,or as I like to call her, Dil<3er(for the story on how the nickname came about <a href="#dil<3er">click here</a>). But now 8 months
later and my pathways cohort has become my second family.

Whether I just need to talk about my day or just relieve some stress and have some fun, my cohort members are always 
there for me, I love them, they are so supportive and understanding, not only that, but they're also genuine.

It's hard finding genuine people in college. People say one things when they're thinking another. When you find that group 
of geunine friends hold on to them, because they will tell you know how things are. And best part, they aren't afraid to tell 
you what may be the hurtful truth because <span class="color">**THEY ARE TRYING TO HELP YOU!!</span>Looking back at all of the
events I have gone on with my cohort, I just can't imagine what my freshman year would have been like without them. They are
just so kindhearted, and definitely funny people (Oh man, every time I'm with them I'm always dying of laughter, sometimes 
even crying of laughter).

 For incoming freshman who plan on commuting to NYU, I definitely recommend applying to be part of Pathways. It's an 
 amazing opportunity, you meet new people/make friends,  AND you go to<strong><span class="color"> **FREE**</span></strong> events throughout the year.
 Did I mention, the events are <strong><span class="color">**FREE**</span></strong>

If you aren't convinced you should go,<span class="color"> JUST DO IT!!</span> Last year, during the commuter's overnight 
retreat, during the scavenger hunt my pathways team got to take a photo with <span class="color2">Andy Karl, the actor that played Rocky in 
Rocky the Musical.</span> Like, it can't hurt to try<br><br>Until next time,<br>~Au Revior, mes amis~
