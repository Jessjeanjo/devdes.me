---
layout: post
title: P01 "THE END IS NEAR"_A MAJOR CHANGE (pun intended)
---

So I'm kicking off my <strong>"The End is Near" </strong>Reflection Series with what I believe was<span class="color">
the largest change of my year,changing my major from Mechanical Engineering to Computer Engineering.</span> After having 
my Intro to Mechanical Engineering class, It became very clear that I was not meant to be a Mechanical Engineer.
I thought the concepts were pleasant, but I just wasn't as interested in the course material as some of my more passionate MechE friends. 

Funny enough, the summer leading up to my freshman year I was very undecided and that <span class="color">freaked me out!</span>
I didn't want to enter college undecided for two main reasons. One, I already had so many credits from the early college classes
I took in high school as well as AP exams, that not knowing wasn't an option<span class="color"> (which is totally ridiculous, 
if you don't know then you just don't know)</span>. And Two, my Dad <span class="color">expected</span> me to KNOW
what I was going to study before starting. Overall, he felt like it was a waste of time and money for me to go to college not 
having any idea of what I wanted to pursue. So yeah, the pressure was on!

The pressure was on, what did I do? Well, I talked to my older brother, he knows me so well, and I'm glad I did. After talking to him he 
basically told me Computer Engineering is the place for me. I was in a bit shocked because he just knew, like based off of 
what I constantly talked about,<span class="color"> like...WHOA!<br></span>

I immediately made the switch<span class="color"> (which was a relatively easy process, kudos NYU)</span> and by spring 2015
(this semester) I was able to start taking my CompE cirriculum classes. I absolutely love my classes and my Intro to Computer 
Engineering course material confirms that this is definitely the field for me. 

<figure align= "middle">
<img src = "http://media.giphy.com/media/2FazpUBmoGaFOiXBK/giphy.gif" alt = "Happy-football" align="middle" height ="323" width ="425">
<figcaption align="middle"><span class="color">My emotions when I think about my radical field</span></figcaption>
</figure>

So, I find myself, what's next? <br><br>Well, so far I have just been diving head first into my field with various tech meet ups, learning 
different languages(which I will talk about later in the series), and learning about chips and circuits.<br><br>With this semester coming to a rapid end, I can 
really start to redirect my focus into some projects and ideas that I want to work on and just 
<strong><span class="color">#TURNUPwithCODING</span></strong><br>I'm excited!<br><br>Until next time,<br>~Au Revoir, mes amis~
