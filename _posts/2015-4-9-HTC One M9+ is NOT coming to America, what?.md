---
layout : post
title : HTC One M9+ is NOT coming to America, what?
---
 
It seems like the HTC One M9+ will NOT be coming to North America and Europe.
I first heard this<span class="color"> #FACT</span>, though it seemed like a rumor at the time, on Twitter.<a href="http://twitter.com/engadget">@engadget</a> tweeted their article 
<i>"HTC doesn't plan on bringing the M9+ to North America or Europe" starting the search"</i>

<figure align="middle">
<img src="http://media.giphy.com/media/itTf5594jwVDW/giphy.gif" alt="Eddie Murphy Coming to America">
<figcaption align="middle">HTC isn't Coming to America? What?</figcaption>
</figure>

<span class="color">Or will they?</span>

Intially hearing this news, I was just as shocked as Eddie Murphy above, why would HTC say such a thing? Was there a dispute? Would this be the end of HTC coming to America?<br>
 As an HTC user, I felt out of the loop, like come on! What will happen later on? What will become 
of HTC One in the UK and USA?

Turns out HTC has released their HTC One M9 in the UK and USA, but will not, in fact, be releasing their plus version in the USA
and UK,believing the regular M9 would be best choice for our needs.

<strong><span class="color2">HTC is excited to bring the HTC One M9+ to customers in China,
where we have worked closely with mobile operators to create a
phone with the right balance of screen size, processor 
performance, software,and radio network compatibility to meet 
consumers' needs. Ranging in other markets will be confirmed 
locally at a later date. The One M9+ is not currently planned
to be released in North America or Europe, where we 
believe our flagship HTC One M9 is the best choice for blazing
fast performance, incredible sound, and network compatibility 
across the broadest range of operators."</span></strong>

I do not entirely agree with HTC not giving USA and UK HTC users the <strong>option</strong>
of purchasing the HTC One M9+ and bask in its Quad HD display

<figure style="float:left;">
<img src="http://blog.htc.com/wp-content/uploads/2015/04/HTC_One_M9+_3V_Silver_Blog-Header.jpg" alt="M9+">
<figcaption align="middle">HTC One M9+</figcaption>
</figure>

<strong>SPEC COMPARISON</strong>
The M9+ has a Quad HD display (2560 x 1440),a drastic increase in sharpness over the M9's standard Full HD display( 1920 x 1080).<br><br>
With the plus, HTC has decided to opt for a octa-core MediaTek MT6795T chip which clocked at up to 2.2GHz. However the new processor
does not show a significant change over the M9's octa-core Qualcomm Snapdragon 810 processor which clocked at up to 2GHz.<br><br>
Now, the camera. The M9 has a 20.7 megapixel snapper, unlike the M9+'s 20 medgapixel Duo Camera.<br><br>Both phones come with 32GB of internal
storage, however it is unclear whether the M9+ will offer a microSD expansion.


<strong>Ultimately...</strong><br>It seems like aside from the Quad HD display, the M9+ and M9 are practically the same.
<i><span class="color2"> Except, the button.Why is there a button on the M9+?</span></i>
For now, I will accept HTC not offering the M9+, but their later released version NEED to have it! After today, 
anything less than Quad HD will <strong>NOT be accepted!</strong><span class="color">(not entirely true, but yah know)</span>
<br><br>Until next time,<br>~Au Revior, mes amis~
