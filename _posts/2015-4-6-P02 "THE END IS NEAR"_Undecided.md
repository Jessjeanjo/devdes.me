---
layout: post
title: THE END IS NEAR_Undecided 
---

All my life I was always told that I needed to know what I was going to do for the rest of my life. So much pressure, 
like how do I even make a decision that massive? <span class="color">How does anyone?</span> Well, from my experience, 
it seems like you just <span class="color">try different things</span>.

In the beginning of my freshman year I was totally for Mechanical Engineering. I planned out my schedule for the next four 
year, class wise, I was set to go! MechE is a pretty general field, so I was all for going with MechE since I knew would get to know 
bit about every field. Not only that, but I always enjoyed physics, which is the essence of MechE.<span class="color"> Life is great, right?</span>

Oh, how wrong I was.<br><br> After the 3rd week in my Intro to MechE course, I began wrestling with the idea of Mechanical
Engineering. I started to realize that although I thought the course pleasant, I <span class="color">wasn't<i> passionate</i></span>
about the material, at all. I didn't enjoy going home and doing my MechE related work. I wasn't even passionate about the 
MechE related posts my MechE friends would tag me in on FaceBook.

<!---<img src = "http://f.ptcdn.info/019/004/000/1365571127-Dancinga-o.gif" alt = "Dancing-Android" height="117" width="140" style="float:left; margin-right:0.25em;">
Could I have continued studying Mechanical Engineering, succesfully?
Well, yeah, but I didn't want to just enjoy my major, I wanted to love my major. Love, something I didn't feel as a MechE major. 
It got to the point where I would try to escape MechE<span class="color">(although, at the time, I didn't realize that I was trying to escape)</span> by going to tech/computing related events. One of which included 
<a href="http://nyc.droidcon.com/2014/#">DroidCon NYC</a>[DroidCon is the official android conference for software updates and android related news and workshops].--->

DroidCon was definitely the turning point for me. After DroidCon I came to the realization that I really don't like MechE as much as I thought I did.
A few weeks later I just sat down, and talked to my brother(For the talk details/results <a href="#post-0504">read P01</a>) and got his advice on the situation.

In an instant I went from being undecided to seeing everything clearly. After facing the truth about the feelings I had for my
MechE related work, life began to fall into place.<br>If you're undecided I suggest trying different fields out and really accessing
how you feel when you partake in that fields' work.<span class="color"> Do you find it enjoyable? Does that field embody the life style you ultimately want to have?</span><br><br>
And that second question was a key factor, because I knew what kind of life I wanted to have, I just didn't know what would get me there.
Also, if you have someone who knows you pretty well, ask them. They may be able to realize a pattern that you were to invovled with to see.<br><br>
Until next time,<br>~Au Revoir, mes amis~
